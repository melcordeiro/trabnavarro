﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiTrabalho.Entidades
{
    //entidade de aula
    public class Aula
    {
        public int Id { get; set; }
        public Funcionario Professor { get; set; }
        //public List<Funcionario> Instrutores { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public Calendario Calendario { get; set; }
    }
}