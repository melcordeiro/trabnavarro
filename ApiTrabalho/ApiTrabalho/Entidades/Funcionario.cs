﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiTrabalho.Entidades
{
    //Entidade do funcionário 
    public class Funcionario
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cargo { get; set; }
        public string Email { get; set; }
        public DateTime DataNascimento {get;set;}
    }
}