﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiTrabalho.Entidades
{
    public class Calendario
    {
        public int Id { get; set; }
        public string DiaSemana { get; set; }
        public TimeSpan Horario { get; set; }
    }
}