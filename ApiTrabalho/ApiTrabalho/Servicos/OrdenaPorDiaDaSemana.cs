﻿using ApiTrabalho.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiTrabalho.Servicos
{
    public class OrdenaPorDiaDaSemana
    {
        public List<Aula> RetornaListaCalendarioSemanalPorAula (string nomeAula)
        {
            //Criação de calendario semanal
            var listCalendario = new List<Calendario>
            {
                new Calendario { Id = 1, DiaSemana = "Sexta-Feira", Horario = new TimeSpan (17,30,0)},
                new Calendario { Id = 2, DiaSemana = "Sexta-Feira", Horario = new TimeSpan (18,30,0)},
                new Calendario { Id = 3, DiaSemana = "Segunda-Feira", Horario = new TimeSpan (17,30,0)},
                new Calendario { Id = 4, DiaSemana = "Sábado", Horario = new TimeSpan (14,0,0)}
            };

            //lista de funcionarios
            var listfuncionarios = new List<Funcionario>
            {
                new Funcionario { Id = 1, Nome = "Fabio Cordeiro da Silva", Cargo = "Professor", Email = "cordeirocsf@hotmail.com", DataNascimento = new DateTime (1978,1,7) },
                new Funcionario { Id = 2, Nome = "Maria Cristina Cardoso", Cargo = "Professor", Email = "cristina.cardoso@gmail.com", DataNascimento = new DateTime (1972,21,10)},
                new Funcionario { Id = 3, Nome = "Valter Doná", Cargo = "Instrutor", Email="valter.dona@gmail.com", DataNascimento = new DateTime (1987,16,8) }
            };

            //criação de aulas
            var aulaAlongamento = new Aula {Id=1,Nome = "Alongamento", Descricao = "Para homens e mulheres", Calendario = listCalendario.First(c => c.Id==1), Professor = listfuncionarios.First(f => f.Id == 2)};
            var aulaSalao = new List<Aula>
            {
                new Aula { Id = 1, Nome = "Salao", Descricao="Intermediário", Calendario = listCalendario.First(c => c.Id == 3), Professor = listfuncionarios.First(f => f.Id ==1)},
                new Aula { Id = 2, Nome = "Salao", Descricao="Iniciante", Calendario = listCalendario.First(c => c.Id == 4), Professor = listfuncionarios.First(f => f.Id ==3)}
            };

            var listaDeAulas = new List<Aula>();
            listaDeAulas.Add(aulaAlongamento);
            listaDeAulas.AddRange(aulaSalao);

            return listaDeAulas;
        }
    }
}