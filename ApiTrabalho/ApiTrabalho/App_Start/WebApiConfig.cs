﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace ApiTrabalho
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // variável formatter recebe os formatadores de tipo de mídia do HttpConfiguration
            var formatters = config.Formatters;
            formatters.Remove(formatters.XmlFormatter); //remove o formato XML

            var jsonSettings = formatters.JsonFormatter.SerializerSettings; //serializar para string
            jsonSettings.Formatting = Formatting.Indented; //realizar identação da string
            jsonSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            formatters.JsonFormatter.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.None;
            config.MapHttpAttributeRoutes();// realizar mapeamento

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
