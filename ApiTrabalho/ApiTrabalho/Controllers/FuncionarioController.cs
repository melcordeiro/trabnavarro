﻿using ApiTrabalho.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace ApiTrabalho.Controllers
{
    public class FuncionarioController : ApiController
    {
        //Lista de funcionários
        static List<Funcionario> funcionarios = new List<Funcionario>
        {
            new Funcionario { Id = 1, Nome = "Fabio Cordeiro da Silva", Cargo = "Professor", Email = "cordeirocsf@hotmail.com", DataNascimento = new DateTime (1978,1,7) },
            new Funcionario { Id = 2, Nome = "Maria Cristina Cardoso", Cargo = "Professor", Email = "cristina.cardoso@gmail.com", DataNascimento = new DateTime (1972,21,10)},
            new Funcionario { Id = 3, Nome = "Valter Doná", Cargo = "Instrutor", Email="valter.dona@gmail.com", DataNascimento = new DateTime (1987,16,8) }
        };

        //Retorna todos os funcionarios cadastrados
        public IHttpActionResult GetAllFuncionarios()
        {
            return Ok(funcionarios);
        }

        //Retorna um funcionario específico buscado pelo Id
        public IHttpActionResult GetFuncionario(int id)
        {
            //FirstOrDefault = retorna o primeiro elemento da sequencia que satisfaz a condição ou valor padrão se não for encontrado elemento
            var funcionario = funcionarios.FirstOrDefault((f) => f.Id == id);
            if (funcionario == null)
                return NotFound();
            return Ok(funcionario);
        }

        //POST para api/Funcionarios 
        public IHttpActionResult Post([FromBody] Funcionario funcionario)
        {
            if(funcionario.Id == 0)
            {
                //OrderBy método sort que ordena os elementos da sequencia em ordem crescente de acordo com a chave
                var indiceFinal = funcionarios.OrderBy(x => x.Id).Last().Id;
                funcionario.Id = indiceFinal + 1; // posiciona o funcionario recebido na ultima posição da lista
            }
            funcionarios.Add(funcionario); // adiciona o funcionario recebido na lista 
            return Ok(funcionarios);
        }

        /*
        //PUT, recebe o id e um objeto do tipo funcionario
        public IHttpActionResult PutFuncionario(int id, Funcionario func)
        {
            func.Id = id; // id do objeto recebe o id enviado por parametro
            if(!Update(func)) // verifica se item não foi atualizado
            {
                throw new HttpResponseException(System.Net.HttpStatusCode.NotFound);
            }

            //retorna sucesso caso atualização tenha acontecido
            return Ok("Funcionário atualizado com sucesso!");
        }

        //Update, recebe objeto do tipo funcionario
        public Boolean Update(Funcionario funcionario)
        {
            if(funcionario == null)
            {
                throw new ArgumentNullException("funcionario"); // se for nulo gera exception indicando que o funcionario é nulo
            }

            //FindIndex procura elemento que tem as informações especificadas na condição, retornando um número baseado em zero da primeira ocorrência dentro da Lista
            // ou -1 caso nenhum elemento seja encontrado 
            var i = funcionarios.FindIndex(f => f.Id == funcionario.Id);
            if (i == -1)
            {
                return false;
            }

            funcionarios.RemoveAt(i); //remove da lista o objeto q possui o mesmo Id encontrado
            funcionarios.Add(funcionario); //adiciona o novo objeto que foi enviado inicialmente 
            return true;
        }

        //DELETE
        public IHttpActionResult Delete(int id)
        {
            //FindIndex procura elemento que tem as informações especificadas na condição, retornando um número baseado em zero da primeira ocorrência dentro da Lista
            // ou -1 caso nenhum elemento seja encontrado
            var i = funcionarios.FindIndex(f => f.Id == id);
            if(i == -1)
            {
                return NotFound();
            }

            //remove da lista o elemento verificado 
            funcionarios.RemoveAt(i);
            return Ok("Funcionário Excluído!");
        }
        */
    }
    
}