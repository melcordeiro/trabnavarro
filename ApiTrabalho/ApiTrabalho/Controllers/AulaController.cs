﻿using ApiTrabalho.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace ApiTrabalho.Controllers
{
    public class AulaController : ApiController
    {
        static List<Aula> aulas = new List<Aula>
        {
            new Aula {Id=1, Nome="Zumba", Descricao = "Para homens e mulheres" },
            new Aula {Id=2, Nome="Tango", Descricao = "Professora argentina" },
            new Aula {Id=3, Nome="Dança de Salão", Descricao="Intermediário" }
        };
        //Retorna todos as aulas cadastrados
        public IHttpActionResult GetAllAulas()
        {
            return Ok(aulas);
        }

        //Retorna uma aula específica buscado pelo Id
        public IHttpActionResult GetAula(int id)
        {
            //FirstOrDefault = retorna o primeiro elemento da sequencia que satisfaz a condição ou valor padrão se não for encontrado elemento
            var aula = aulas.FirstOrDefault((f) => f.Id == id);
            if (aula == null)
                return NotFound();
            return Ok(aula);
        }

        //POST para api/Aulas 
        public IHttpActionResult Post([FromBody] Aula aula)
        {
            if (aula.Id == 0)
            {
                //OrderBy método sort que ordena os elementos da sequencia em ordem crescente de acordo com a chave
                var indiceFinal = aulas.OrderBy(x => x.Id).Last().Id;
                aula.Id = indiceFinal + 1; // posiciona a aula recebido na ultima posição da lista
            }
            aulas.Add(aula); // adiciona a aula recebido na lista 
            return Ok(aulas);
        }
    }
}